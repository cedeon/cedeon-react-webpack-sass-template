import cs from 'style-loader!css!sass!./App.scss';
import React from 'react';

class App extends React.Component {
   render() {
     return (
         <div className="cedeon-App">
           Hello World!
         </div>
      );
   }
}

export default App;
