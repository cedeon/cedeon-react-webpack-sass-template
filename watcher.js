import watch from 'watch-glob';
import cp from 'child_process';
import browserSync from 'browser-sync';

let fileMap = [
  { glob: 'src/index.html', npmAction: ['run', 'webpack'] },
  { glob: 'src/components/**/*', npmAction: ['run', 'webpack'] },
  { glob: 'src/scss/**/*', npmAction: ['run', 'build:css:dev'] },
  { glob: 'src/*.scss', npmAction: ['run', 'build:css:dev']  }
];

var addWatch = function(fm) {
  watch(fm.glob, { callbackArg: 'relative' }, function(fp) {
    console.log('[File Change]: '+ fp );
    console.log('[Exec]: '+ fm.npmAction );
    cp.spawn('npm', fm.npmAction, { stdio: 'inherit' });
  });
};




for (let i=0; i<fileMap.length; i++) {
  addWatch(fileMap[i]);
}

// Start the server
var bs = browserSync.create();
bs.init({
  server: "./dest"
});



// Listen to change events on HTML and reload
bs.watch("dest/*.html").on("change", bs.reload);

// Provide a callback to capture ALL events to CSS
// files - then filter for 'change' and reload all
// css files on the page.
bs.watch("dest/*.css", function (event, file) {
    if (event === "change") {
        bs.reload("*.css");
    }
});

// Provide a callback to capture ALL events to js
// files - then filter for 'change' and reload all
// css files on the page.
bs.watch("dest/*.js", function (event, file) {
    if (event === "change") {
        bs.reload("*.js");
    }
});
